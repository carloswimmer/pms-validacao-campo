# pms-validacao-campo

Componente de validação de campo para aplicações Vue.

## Instalação

```bash
npm install git+https://dev.santos.sp.gov.br/publico/pms-validacao-campo.git#master --save
```

## Props

| Obrigatório | Nome | Tipo | Default | Formato |
|-------------|------|------|---------|---------|
| **Sim** | (1) nome | `String` | | livre |
| **Sim** | rotulo | `String` | | livre |
| Não | icone | `Boolean` | `true` | |
| Não | classe | `String` | '' (vazio) | |
| Não | (2) erros | `Array` | `undefined` | |

### Observações

**(1)** Lembre-se de que o valor da *prop* `nome` deverá equivaler ao valor do atributo `id` que você deverá inserir no `input` utilizado, devido ao fato que esta *prop* `nome` será utilizada no atributo `for` da `label` existente no componente.

**(2)** Erros customizados.

### Exemplo de utilização

**1. erros**

Se optar por definir você mesmo uma mensagem de erro em detrimento da mensagem que viria através do [Vee Validate](https://baianat.github.io/vee-validate), defina da seguinte forma:

```javascript
const erros = {
  txt_Email: ['Algo está errado com o email'],
  txt_Senha: ['A senha inserida é muito fraca']
}
```

> No exemplo acima, tanto o atributo `txt_Email` quanto o `txt_Senha` referem-se ao campo `name` do `input`

## Utilização

O exemplo abaixo apresenta a forma como você deverá utilizar o componente:

```html
<pms-validacao-campo nome="txt_Email" rotulo="Email">
 <input v-validate="'required|min:5'" name="txt_Email" type="email">
</pms-validacao-campo>
```

> **Observação:** A diretiva `v-validate` refere-se às regras de validação disponibilizadas pelo já mencionado **Vee Validate**. Para saber mais a respeito das regras de validação disponíveis, acesse [esta seção da documentação da biblioteca](https://baianat.github.io/vee-validate/guide/rules.html).

O modo como o exemplo acima foi escrito resultará -- caso existam erros -- na exibição dos erros encontrados pela biblioteca de validação. Caso queira fazer com que erros pré-definidos por você sejam exibidos (assim como foi feito no tópico [Exemplo de Utilização](#exemplo-de-utilização)), basta enviar a propriedade `erros` para o componente da seguinte forma:

```html
<pms-validacao-campo :erros="erros.txt_Email">
 <!-- ... -->
</pms-validacao-campo>
```
