export default {
  erros: Array,
  
  classe: {
    type: String,
    default: ''
  },
  
  icone: {
    type: Boolean,
    default: true
  },
  
  nome: {
    type: String,
    required: true
  },
  
  rotulo: {
    type: String,
    required: true
  }
}