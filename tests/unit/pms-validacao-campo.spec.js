/* eslint-disable */
import { shallowMount } from '@vue/test-utils'
import PmsValidacaoCampo from '@/pms-validacao-campo.vue'

let validacao = {
  campo: {
    icone: true,
    nome: 'Nome X',
    rotulo: 'Rótulo X',
    classe: 'classe-exemplo'
  },
  
  erro: {
    field: 'Campo X',
    msg: 'Mensagem Z' 
  }
}

let dados = {
  ...validacao.campo,
  erros: [ validacao.erro ]
}

let wrapper
let wrapperDefault = true

describe('pms-validacao-campo', () => {
  it('inicia ativo', () =>
    expect(wrapper.vm.ativo).toEqual(true)
  )
  
  it('possui label com atributo `for` definido', () => {
    const label = wrapper.find('.control-label')
    expect(label.attributes('for')).toEqual(validacao.campo.nome)
  })
  
  it('possui classes definidas na div inicial', () => {
    let classes, elemento
    
    elemento = wrapper.find('.form-group')
    classes  = wrapper.vm.classes
    
    expect(elemento.classes()).toContain(classes)
  })
  
  it('possui um rótulo definido', () => {
    const elemento = wrapper.find('.control-label')
    
    expect(elemento.exists()).toBe(true)
    expect(elemento.text()).toEqual(validacao.campo.rotulo)
    
    wrapperDefault = false
  })
  
  it('possui a seção de exibição de erros de validação', () =>
    expect(wrapper.find('.validation-errors').exists()).toBe(true)
  )
  
  it('indica que existe problema', () => {
    const existe = wrapper.vm.existeProblema
    expect(existe).toEqual(true)
  })
  
  it('retorna problemas de validação', () => {
    const problemas = {
      itens: wrapper.vm.problemas,
      existem: false
    }
    
    problemas.existem = problemas.itens.length > 0
    
    expect(problemas.existem).toEqual(true)
    expect(typeof problemas.itens).toBe('object')
  })
  
  it('retorna classes relacionadas ao status da validação', () => {
    const classes = {
      retornadas: wrapper.vm.classes,
      esperadas: `has-error ${validacao.campo.classe}`
    }
    
    expect(classes.retornadas).toEqual(classes.esperadas)
  })
  
  it('indica que os valores do rótulo, array problema e ícone estão definidos', () => {
    expect(wrapper.vm.elementosDefinidos).toEqual(true)    
  })
  
  describe('possui erro de validação que', () => {
    it('está definido', () =>
      expect(wrapper.find('.help-block').exists()).toBe(true)
    )
    
    it('exibe problema constatado', () => {
      const elemento = wrapper.find('.help-block p')
      const problema = validacao.erro.msg
      
      expect(elemento.text()).toMatch(problema)
    })
    
    it('disponibiliza opção de fechá-lo', () =>
      expect(wrapper.find('.glyphicon-remove').exists()).toBe(true)
    )
    
    it('é fechado através de click em <span>', () => {
      const icone = wrapper.find('.glyphicon-remove')
      icone.trigger('click')
      
      expect(wrapper.vm.ativo).toEqual(false)
    })
    
    it('possui ícone de exclamação (!)', () => {
      const icone = wrapper.find('.fa-exclamation')
      expect(icone.exists()).toEqual(true)
    })
  })
})

beforeEach(() => {
  const props = [{ ...validacao.campo }, { ...dados }]
  const propsData = props[wrapperDefault ? 0 : 1]
  
  wrapper = shallowMount(PmsValidacaoCampo, { propsData })
})