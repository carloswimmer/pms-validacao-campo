import ptBR from 'vee-validate/dist/locale/pt_BR';
import Vue from 'vue';
import VeeValidate, { Validator } from 'vee-validate';

var props = {
  erros: Array,
  
  classe: {
    type: String,
    default: ''
  },
  
  icone: {
    type: Boolean,
    default: true
  },
  
  nome: {
    type: String,
    required: true
  },
  
  rotulo: {
    type: String,
    required: true
  }
};

//

Vue.use(VeeValidate);

var script = {
  name: "PmsValidacaoCampo",
  props: props,
  
  data: function () { return ({
    ativo: true
  }); },
  
  methods: {
    fecha: function fecha() {
      this.ativo = false;
    },
  },
  
  computed: {
    problemas: function problemas() {
      var this$1 = this;

      var problemas = [];
      
      if (!this.erros) {
        this.errors.items.map(function (erro) {
          if (erro.field === this$1.nome) {
            problemas.push(erro.msg);
            this$1.ativo = true;
          }
        });
      }
      else {
        this.erros.forEach(function (erro) {
          problemas.push(erro);
          this$1.ativo = true;
        });
      }
      
      return (problemas.length > 0) ? problemas : false
    },
    
    existeProblema: function existeProblema() {
      return this.problemas.length >= 1
    },
    
    classes: function classes() {
      var erro = this.problemas && this.ativo ? 'has-error ' : '';
      return (erro + " " + (this.classe))
    },
    
    elementosDefinidos: function elementosDefinidos() {
      return this.problemas && this.rotulo && this.icone
    }
  },
  
  created: function created() {
    Validator.localize('pt_BR', ptBR);
  },
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
/* server only */
, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
  if (typeof shadowMode !== 'boolean') {
    createInjectorSSR = createInjector;
    createInjector = shadowMode;
    shadowMode = false;
  } // Vue.extend constructor export interop.


  var options = typeof script === 'function' ? script.options : script; // render functions

  if (template && template.render) {
    options.render = template.render;
    options.staticRenderFns = template.staticRenderFns;
    options._compiled = true; // functional template

    if (isFunctionalTemplate) {
      options.functional = true;
    }
  } // scopedId


  if (scopeId) {
    options._scopeId = scopeId;
  }

  var hook;

  if (moduleIdentifier) {
    // server build
    hook = function hook(context) {
      // 2.3 injection
      context = context || // cached call
      this.$vnode && this.$vnode.ssrContext || // stateful
      this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
      // 2.2 with runInNewContext: true

      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
      } // inject component styles


      if (style) {
        style.call(this, createInjectorSSR(context));
      } // register component module identifier for async chunk inference


      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier);
      }
    }; // used by ssr in case component is cached and beforeCreate
    // never gets called


    options._ssrRegister = hook;
  } else if (style) {
    hook = shadowMode ? function () {
      style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
    } : function (context) {
      style.call(this, createInjector(context));
    };
  }

  if (hook) {
    if (options.functional) {
      // register for functional component in vue file
      var originalRender = options.render;

      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context);
        return originalRender(h, context);
      };
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate;
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
    }
  }

  return script;
}

var normalizeComponent_1 = normalizeComponent;

var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
  return function (id, style) {
    return addStyle(id, style);
  };
}
var HEAD;
var styles = {};

function addStyle(id, css) {
  var group = isOldIE ? css.media || 'default' : id;
  var style = styles[group] || (styles[group] = {
    ids: new Set(),
    styles: []
  });

  if (!style.ids.has(id)) {
    style.ids.add(id);
    var code = css.source;

    if (css.map) {
      // https://developer.chrome.com/devtools/docs/javascript-debugging
      // this makes source maps inside style tags work properly in Chrome
      code += '\n/*# sourceURL=' + css.map.sources[0] + ' */'; // http://stackoverflow.com/a/26603875

      code += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) + ' */';
    }

    if (!style.element) {
      style.element = document.createElement('style');
      style.element.type = 'text/css';
      if (css.media) { style.element.setAttribute('media', css.media); }

      if (HEAD === undefined) {
        HEAD = document.head || document.getElementsByTagName('head')[0];
      }

      HEAD.appendChild(style.element);
    }

    if ('styleSheet' in style.element) {
      style.styles.push(code);
      style.element.styleSheet.cssText = style.styles.filter(Boolean).join('\n');
    } else {
      var index = style.ids.size - 1;
      var textNode = document.createTextNode(code);
      var nodes = style.element.childNodes;
      if (nodes[index]) { style.element.removeChild(nodes[index]); }
      if (nodes.length) { style.element.insertBefore(textNode, nodes[index]); }else { style.element.appendChild(textNode); }
    }
  }
}

var browser = createInjector;

/* script */
var __vue_script__ = script;

/* template */
var __vue_render__ = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "div",
    {
      class: "form-group has-feedback " + _vm.classes,
      attrs: { "data-pms": "validacao-campo" }
    },
    [
      _vm.rotulo
        ? _c(
            "label",
            { staticClass: "control-label", attrs: { for: _vm.nome } },
            [_vm._v("\n    " + _vm._s(_vm.rotulo) + "\n  ")]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm._t("default"),
      _vm._v(" "),
      _vm.ativo
        ? _c("section", { staticClass: "validation-errors" }, [
            _c("span", {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.elementosDefinidos,
                  expression: "elementosDefinidos"
                }
              ],
              staticClass: "glyphicon glyphicon-remove form-control-feedback",
              attrs: { "aria-hidden": "true" },
              on: { click: _vm.fecha }
            }),
            _vm._v(" "),
            _vm.problemas
              ? _c(
                  "span",
                  { staticClass: "help-block" },
                  _vm._l(_vm.problemas, function(problema) {
                    return _c("p", { key: _vm.problemas.indexOf(problema) }, [
                      _vm.existeProblema
                        ? _c("i", {
                            staticClass: "fa fa-exclamation",
                            attrs: { "aria-hidden": "true" }
                          })
                        : _vm._e(),
                      _vm._v("\n        " + _vm._s(problema) + "\n      ")
                    ])
                  }),
                  0
                )
              : _vm._e()
          ])
        : _vm._e()
    ],
    2
  )
};
var __vue_staticRenderFns__ = [];
__vue_render__._withStripped = true;

  /* style */
  var __vue_inject_styles__ = function (inject) {
    if (!inject) { return }
    inject("data-v-42856e02_0", { source: "[data-pms='validacao-campo'] .help-block p {\n  margin: 0;\n}\n[data-pms='validacao-campo'] .form-control-feedback {\n  cursor: pointer;\n  pointer-events: all !important;\n}\n", map: {"version":3,"sources":["pms-validacao-campo.vue"],"names":[],"mappings":"AAAA;EACE,SAAS;AACX;AACA;EACE,eAAe;EACf,8BAA8B;AAChC","file":"pms-validacao-campo.vue","sourcesContent":["[data-pms='validacao-campo'] .help-block p {\n  margin: 0;\n}\n[data-pms='validacao-campo'] .form-control-feedback {\n  cursor: pointer;\n  pointer-events: all !important;\n}\n"]}, media: undefined });

  };
  /* scoped */
  var __vue_scope_id__ = undefined;
  /* module identifier */
  var __vue_module_identifier__ = undefined;
  /* functional template */
  var __vue_is_functional_template__ = false;
  /* style inject SSR */
  

  
  var pmsValidacaoCampo = normalizeComponent_1(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    browser,
    undefined
  );

export default pmsValidacaoCampo;
