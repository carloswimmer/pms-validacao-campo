import Vue from 'vue'
import App from './App.vue'

import './main.less'
import '@pms/pms-tema/dist/pms-tema.min.css'

new Vue({ render: h => h(App) }).$mount('#app')